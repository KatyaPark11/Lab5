﻿using System;
using System.Globalization;

namespace Lab5
{
    class TheSecondExercise
    {
        public static void DoTheSecond()
        {
            Console.Write("Введите что угодно, но знайте, что желательно, чтобы это было целое число: ");

            var input = Console.ReadLine();

            if (input == String.Empty)
            {
                Console.WriteLine("Ничего не было введено...");
                return;
            }

            if (input[0] == '-' || input[0] == '+') input = input.Remove(0, 1);
            if (double.TryParse(input.Replace(',', '.'), NumberStyles.Any,
                CultureInfo.InvariantCulture, out double doubleNum) && 
                doubleNum == Math.Round(doubleNum))
            {
                Console.WriteLine("Введённое число является целым");
                return;
            }

            foreach (char c in input)
            {
                if (!Char.IsNumber(c))
                {
                    Console.WriteLine("Данная запись не представляет собой целое число(");
                    return;
                }
            }
            Console.WriteLine("Введённое число является целым");
        }
    }
}
