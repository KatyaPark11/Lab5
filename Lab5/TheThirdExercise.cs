﻿using System;
using System.Globalization;

namespace Lab5
{
    class TheThirdExercise
    {
        public static void DoTheThird()
        {
            string[] numArray = InputToArray();
            if (numArray == null) return;
            var result = new long[numArray.Length];

            for (int i = 0; i < numArray.Length; i++)
            {
                if (Int32.TryParse(numArray[i], out int intNum))
                {
                    if (intNum >= 0) result[i] = Factorial(intNum);
                    else result[i] = intNum;
                }
                else if (double.TryParse(numArray[i].Replace(',', '.'), NumberStyles.Any, 
                    CultureInfo.InvariantCulture, out double doubleNum))
                {
                    doubleNum = Math.Round(doubleNum, 2);
                    int commaIndex = doubleNum.ToString().IndexOf(',');
                    int pointIndex = doubleNum.ToString().IndexOf('.');
                    result[i] = (pointIndex != -1) ? long.Parse(doubleNum.ToString()[(pointIndex + 1)..]) 
                                         : long.Parse(doubleNum.ToString()[(commaIndex + 1)..]);
                }
            }

            Console.Clear();
            Console.WriteLine($"Исходный массив: {String.Join(" ", numArray)}");
            Console.WriteLine($"Новый массив: {String.Join(" ", result)}");
        }

        public static long Factorial(int number)
        {
            long result = 1;
            if (number == 0) return 1;

            for (int i = 1; i < number; i++)
            {
                result *= i;
            }
            return result;
        }

        public static string[] InputToArray()
        {
            string inputArr = "";
            while (true)
            {
                Console.WriteLine("Введите число (Если ввод массива завершён, просто нажмите Enter): ");
                string input = Console.ReadLine();

                if (input == "") break;
                else if (input[0] == '0' && !input.Contains('.') && !input.Contains(',')) input = input.TrimStart('0');
                else if (input.Contains('.') && input.Contains(','))
                {
                    input = input.TrimStart('0');
                    if (input[0] == '.' || input[0] == ',') input = "0" + input;
                }

                if (input == "") input = "0";

                if (IsDigit(input)) inputArr += input + " ";
            }

            if (inputArr == "") return null;
            return inputArr.Trim().Split(' ');
        }

        public static bool IsDigit(string str)
        {
            if (str[0] == '-' || str[0] == '+') str = str.Remove(0, 1);
            int pointCount = 0;

            foreach (char c in str)
            {
                if (pointCount > 1) return false;
                else if (str[^1] == '.' || str[^1] == ',' || str[0] == '.' || str[0] == ',') return false;
                else if (c == '.' || c == ',')
                {
                    pointCount++;
                    continue;
                }

                if (!Char.IsNumber(c))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
