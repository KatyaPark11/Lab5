﻿using System;

namespace Lab5
{
    class Program
    {
        static void Main()
        {
            while (true)
            {
                Console.WriteLine("Which exercise do you want to test? (first, second, third)");
                string exerciseNum = Console.ReadLine();
                Console.Clear();
                
                switch (exerciseNum)
                {
                    case "first":
                        TheFirstExercise.DoTheFirst();
                        break;
                    case "second":
                        TheSecondExercise.DoTheSecond();
                        break;
                    case "third":
                        TheThirdExercise.DoTheThird();
                        break;
                    default:
                        Console.WriteLine("There is wrong value... Try again.");
                        continue;
                }

                Console.WriteLine("Do you want to continue? (Write no if your answer is \"no\", otherwise write whatever.))");
                string answer = Console.ReadLine();
                Console.Clear();

                if (answer == "no") break;
            }
        }
    }
}