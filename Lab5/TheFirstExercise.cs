﻿using System;

namespace Lab5
{
    class TheFirstExercise
    {
        public static void DoTheFirst()
        {
            var lastElem = float.NaN;

            while (true)
            {
                Console.WriteLine("Введите что-нибудь (желательно всё-таки число): ");
                var input = Console.ReadLine();
                Console.Clear();

                if (input == "q")
                {
                    Console.WriteLine("Ой! Вы наткнулись на супер секретный символ... Теперь программа отказывается работать(");
                    return;
                }
                else if (Int32.TryParse(input, out int intNum))
                {
                    Console.WriteLine($"Ого, что это? Да это же символ под введённым Вами номером: {(char)intNum}");
                    lastElem = intNum;
                }
                else if (float.TryParse(input, System.Globalization.NumberStyles.Float,
                System.Globalization.CultureInfo.InvariantCulture, out float floatNum))
                {
                    if (floatNum == lastElem)
                    {
                        Console.WriteLine("В мире этой программы не принято вводить два одинаковых числа подряд, " +
                            "если хотя бы одно из них куда-то плывёт... Извините, меня заставили!");
                        return;
                    }

                    Console.WriteLine("Хм... Неплохое чиселко... За него Вам позволено повторить ввод числа!");
                    lastElem = floatNum;
                }
                else Console.WriteLine("Вы написали то, что ни к чему Вас не привело... Разве что к повторному вводу!");
            }
        }
    }
}
